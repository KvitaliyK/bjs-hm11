// Теоретичні питання
// 1. Що таке події в JavaScript і для чого вони використовуються?
// Події - це спеціальні сигнали, які інформують про те, що відбулось

// 2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
// click: відбувається, коли користувач клацає на елемент.
// dblclick: відбувається, коли користувач двічі клацає на елемент.
// mouseover: відбувається, коли курсор миші наводиться на елемент.
// mouseout: відбувається, коли курсор миші залишає елемент.
// mousemove: відбувається при русі курсору миші над елементом.

// 3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
// Подія "contextmenu" в JavaScript відбувається, коли користувач клацає правою кнопкою миші на елементі.
// Ця подія дозволяє розробникам замінити стандартне контекстне меню своїм власним або виконати якісь інші дії у відповідь на клацання правою кнопкою миші.


// Практичні завдання
// 1. Додати новий абзац по кліку на кнопку:
// По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і
// додайте його до розділу <section id="content">

const button = document.createElement('button');
button.textContent = 'Click Me';
button.id = 'btn-click';
document.body.append(button);

const contentSection = document.createElement('section');
contentSection.id = 'content';
document.body.append(contentSection);

button.addEventListener('click', () => {
    const paragraph = document.createElement('p');
    paragraph.textContent = 'New Paragraph'
    contentSection.append(paragraph)
})


// 2. Додати новий елемент форми із атрибутами:
// Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
// По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути,
// наприклад, type, placeholder, і name. та додайте його під кнопкою.

const newButton = document.createElement('button');
newButton.id = 'btn-input-create';
newButton.textContent = 'Create Input';

const section = document.createElement('section');
const footer = document.createElement('footer');
document.body.append(footer);
footer.before(section);

section.append(newButton);

newButton.addEventListener('click', () => {
    const input = document.createElement('input');
    input.type = 'text';
    input.placeholder = 'Enter text...';
    input.name = 'input-text';

    newButton.after(input);
});

